import React, { PureComponent } from 'react';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


import ConsoleContainer from './containers/console-container/ConsoleContainer';
import LoginContainer from './containers/login-container/LoginContainer';


class App extends PureComponent {
  render() {
    return (
    <Router>
        <div className="container">
          <Switch>
              <Route path='/login' component={LoginContainer} />
              <Route path='/home'  component={ConsoleContainer} />
              <Route path='/' component={LoginContainer} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;