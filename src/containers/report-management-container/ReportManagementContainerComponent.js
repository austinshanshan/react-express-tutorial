import React, { PureComponent } from 'react';
import './ReportManagementContainerComponent.css';

import EditComponent from '../../components/EditComponent';


class ReportManagementContainerComponent extends PureComponent {
  render() {
    return (
        <div className="docker-management-container">
          <EditComponent/>
        </div>
    );
  }
}

export default ReportManagementContainerComponent;