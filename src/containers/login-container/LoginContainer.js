import React, { PureComponent } from 'react';
import './LoginContainer.css';

import LoginComponent from '../../components/LoginComponent';


class LoginContainer extends PureComponent {
  render() {
    return (
        <div className="login-container">
          <LoginComponent/>
        </div>
    );
  }
}

export default LoginContainer;