import React, { Component } from 'react';
import './ConsoleContainer.css';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import DockerManagementContainerComponent from '../container-management-container/DockerManagementContainerComponent'
import ReportManagementContainerComponent from '../report-management-container/ReportManagementContainerComponent';

class ConsoleContainer extends Component {
  render() {
    return (
        <Router>
            <div className="container">
              <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand">React Express App</a>
                <div className="collapse   navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                          <Link to={'/docker-containers'} className="nav-link">Containers </Link>
                        </li>     
                        <li className="nav-item">
                          <Link to={'/report-management'} className="nav-link">Reports </Link>
                        </li>                
                    </ul>
                </div>
              </nav>
                <Switch>
                    <Route exact path='/docker-containers' component={DockerManagementContainerComponent} />
                    <Route exact path='/report-management' component={ReportManagementContainerComponent} />
                </Switch>
              </div>
          </Router>
    );
  }
}

export default ConsoleContainer;