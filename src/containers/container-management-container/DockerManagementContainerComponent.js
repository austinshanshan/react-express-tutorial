import React, { PureComponent } from 'react';
import './DockerManagementContainerComponent.css';


import ContainerListComponent from '../../components/ContainerListComponent';


class DockerManagementContainerComponent extends PureComponent {
  render() {
    return (
        <div className="docker-management-container">
          <ContainerListComponent/>
        </div>
    );
  }
}

export default DockerManagementContainerComponent;