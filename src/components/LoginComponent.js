import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

class LoginComponent extends PureComponent {

  constructor(props) {

    super(props);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.login = this.login.bind(this);

    this.state = {
      username: '',
      password: '',
      redirectToReferrer: false
    }

  }

  onChangeUsername(e) {
    this.setState({
        username: e.target.value
    });
  }
  onChangePassword(e) {
    this.setState({
        password: e.target.value
    });
  }

  login() {
      const user = {
        email: this.state.username,
        password: this.state.password
      };
      console.log(user.email);
      axios.post('http://localhost:4200/user/login', user)
      .then(res => {
        if(res.status === '200'){
            console.log(res.status)
        }
      });
      this.setState({ 
        username: '',
        password: '',
        redirectToReferrer: true 
      });
  }

  render() {
    const { from } = { from: { pathname: '/home' } }
    const { redirectToReferrer } = this.state;
    
    if (redirectToReferrer) {
      return (
        <Redirect to={from} />
      )
    }

    return (
      <div>
        <form onSubmit={this.login}>
          <h3>Sign in</h3>
          <input type="text" value={this.state.username} ref="username" placeholder="enter you username" onChange={this.onChangeUsername}/>
          <input type="password" value={this.state.password}  ref="password" placeholder="enter password" onChange={this.onChangePassword}/>
          <input type="submit" value="Login" />
        </form>
      </div>
    )
  }
}

export default LoginComponent;